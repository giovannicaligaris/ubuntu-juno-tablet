SHELL := /bin/bash
# Install Juno-Tablet

DESTDIR=debian/juno-tablet

install-core:
	install -dm755 $(DESTDIR)/usr/bin
	install -dm755 $(DESTDIR)/usr/sbin
	install -dm755 $(DESTDIR)/usr/share/suspend-then-hibernate
	install -dm755 $(DESTDIR)/etc/xdg/autostart/
	install -dm755 $(DESTDIR)/etc/udev/rules.d/
	install -dm755 $(DESTDIR)/etc/udev/hwdb.d/
	install -dm755 $(DESTDIR)/etc/systemd/system
	install -dm755 $(DESTDIR)/etc/initramfs-tools
	install -dm755 $(DESTDIR)/usr/share/glib-2.0/schemas/
	install -dm755 $(DESTDIR)/etc/apt/sources.list.d/
	install -dm755 $(DESTDIR)/etc/environment.d/
	install -dm755 $(DESTDIR)/etc/apt/apt.conf.d
	install -dm755 $(DESTDIR)/etc/systemd/logind.conf.d
	install -dm755 $(DESTDIR)/etc/systemd/sleep.conf.d
	install -dm755 $(DESTDIR)/etc/pulse/default.pa.d/
	install -dm755 $(DESTDIR)/usr/share/junocomp/
	install -dm755 $(DESTDIR)/usr/lib/systemd/system-sleep
	install -Dpm 0755 check-battery $(DESTDIR)/usr/bin/check-battery
	install -Dpm 0755 turbo/turbo-on $(DESTDIR)/usr/bin/turbo-on
	install -Dpm 0755 turbo/turbo-off $(DESTDIR)/usr/bin/turbo-off
	install -Dpm 0755 turbo/turbo-stat $(DESTDIR)/usr/bin/turbo-stat
	install -Dpm 0644 20_juno-debian-settings.gschema.override $(DESTDIR)/usr/share/glib-2.0/schemas/20_juno-debian-settings.gschema.override
	install -Dpm 0644 61-sensor-local.hwdb $(DESTDIR)/etc/udev/hwdb.d/61-sensor-local.hwdb
	install -Dpm 0644 rules/squeekboard.rules $(DESTDIR)/etc/udev/rules.d/squeekboard.rules
	install -Dpm 0644 juno.pa $(DESTDIR)/etc/pulse/default.pa.d/juno.pa
	install -Dpm 0644 rules/external-display-power-profile.rules $(DESTDIR)/etc/udev/rules.d/external-display-power-profile.rules
	install -Dpm 0755 juno-monitor $(DESTDIR)/usr/bin/juno-monitor
	install -Dpm 0644 fan-blacklist.conf $(DESTDIR)/etc/modprobe.d/fan-blacklist.conf
	install -Dpm 0644 hid-blacklist.conf $(DESTDIR)/etc/modprobe.d/hid-blacklist.conf
#	install -Dpm 0755 juno-grub $(DESTDIR)/usr/share/junocomp/juno-grub
	install -Dpm 0755 suspend-usb-keyboard $(DESTDIR)/usr/lib/systemd/system-sleep/suspend-usb-keyboard

install: install-core

uninstall:
	rm -f $(DESTDIR)/usr/bin/check-battery
	rm -f $(DESTDIR)/usr/bin/turbo-on
	rm -f $(DESTDIR)/usr/bin/turbo-off
	rm -f $(DESTDIR)/usr/bin/turbo-stat
	rm -f $(DESTDIR)/usr/bin/restore-alsa
	rm -f $(DESTDIR)/etc/initramfs-tools/resume
	rm -f $(DESTDIR)/usr/share/glib-2.0/schemas/20_juno-debian-settings.gschema.override
	rm -f $(DESTDIR)/etc/udev/hwdb.d/61-sensor-local.hwdb
	rm -f $(DESTDIR)/etc/udev/rules.d/power-profiles.rules
	rm -f $(DESTDIR)/etc/udev/rules.d/squeekboard.rules
	rm -f $(DESTDIR)/etc/pulse/default.pa.d/juno.pa
	rm -f $(DESTDIR)/etc/udev/rules.d/external-display-power-profile.rules
	rm -f $(DESTDIR)/usr/bin/juno-monitor
	rm -f $(DESTDIR)/usr/share/suspend-then-hibernate/juno-restore-alsa
	rm -f $(DESTDIR)/etc/modprobe.d/fan-blacklist.conf
	rm -f $(DESTDIR)/etc/modprobe.d/hid-blacklist.conf
	rm -f $(DESTDIR)/usr/lib/systemd/system-sleep/suspend-usb-keyboard